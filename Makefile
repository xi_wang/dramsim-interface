



src = $(wildcard *.c)
obj = $(src:.c=.exe)
CC=gcc

all: $(obj)


%.exe:%.c
	$(CC)  -o $@ $< $(CFLAGS) -O0


.PHONY: clean

clean:
	rm -f ./*.o ./*.exe
