# dramsim-interface

LAB 2 Interface

The cpu.c file is an example that will communicate with DRAMsim3 based on the interface.h

All the communications between CPU and DRAMsim are non-blocking. 

As the FIFO has size limit, you need to send request and check responses every single cycle. You can compare the address to determine which cahce miss can be addressed and record the corresponding returned cycle. We can free a reservation station pending for memory accesses if the CPU cycle reaches the corresponding recorded value.

