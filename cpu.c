#include "interface.h"

#define COUNT 10000


int main(){

		/*Var Init*/
		int 			i 								= 0;
		int 			j 								= 0;
		int 			ret 							= 1;
		int 			ret1 							= 1;
		uint64_t  addr 							= 0x40;

		//for(i = 0; i < 10000; i++, addr += 64)
			//printf("%016lx WRITE %d \n", addr, i);
	//	return 0;

		// FIFO of Requests from CPU to memory
		char 	*rqst_to_memory 	= "./rqst_to_memory";
		// FIFO of Responses from memory to CPU
		char 	*resp_to_cpu 		= "./resp_to_cpu";

		Setup(rqst_to_memory,resp_to_cpu);

		// Requests and Responses counts are identical
		while( i < COUNT || j < COUNT ){

			if(i < COUNT)	{
				if(ret > 0){
					addr += 64;
    			sprintf( addr_send, "%016lx", addr );
    			sprintf( cycle_send, "%d", i );
					strcat(strcat(addr_send, cmd_w), cycle_send);
					i++;
				}
    		ret = SendRqst( addr_send );
				while(ret <= 0)
					ret = SendRqst( addr_send );
				
#ifdef DEBUG
				printf("CPU::MFD WRITE COUNT %d, %s\n", i, addr_send);
#endif
			}

			if(j < COUNT){
#ifdef DEBUG
				printf("CPU::CFD Reading\n");
#endif
				ret1 = RecvResp(addr_recv);
				if(ret1 > 0){
#ifdef DEBUG
					printf("CPU::CFD READ COUNT %d, RETURN %d, Response: %s\n", j, ret1, addr_recv);
#endif
					j++;
			}
#ifdef DEBUG
			else
				printf("CPU::READ PIPE RETURN %d\n", ret1);
#endif
		 }
		}

				
	  Terminate();
	  return 0;
}





